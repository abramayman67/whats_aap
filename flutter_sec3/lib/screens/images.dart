import 'package:flutter/material.dart';

class imageScreens extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 100,
          ),
          Center(
            child: Card(
              elevation: 50,
              //color: Colors.red,
              shadowColor: Colors.grey,
              child: Image.network("https://tse4.mm.bing.net/th?id=OIP.P8n9weKzpAex4RLWzYk9IgHaEK&pid=Api&P=0"
              ,width: 200,
                height: 200,
                //fit: BoxFit.cover,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Image.asset('assets/drawable/egypt.png',
          width: 200,
            height: 200,
          ),

          Align(
            alignment: AlignmentDirectional.topStart,
            child: CircleAvatar(
             // backgroundColor:Colors.red ,
             // foregroundColor: Colors.red,
              radius: 100,
              backgroundImage: NetworkImage("https://tse4.mm.bing.net/th?id=OIP.P8n9weKzpAex4RLWzYk9IgHaEK&pid=Api&P=0"),
            ),
          )
        ],
      ),
    );
  }
}
